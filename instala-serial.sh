#/bin/bash

# apt update
# apt install git -y
# git clone https://gitlab.com/wittemberg1980/serial-startwo.git

apt update
apt install build-essential ssh unrar -y
#nano /etc/ssd/sshd.config
cd /etc/ssh
find ./ -type f -exec sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' {} \;
service ssh restart

cd /home/startwo/Downloads/serial-startwo
unrar e -y ax99100.rar
make
make install
dmesg | grep tty
